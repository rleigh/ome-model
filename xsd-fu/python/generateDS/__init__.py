"""
Pseudo-package to import generateDS.
"""

from __future__ import absolute_import
from . import generateDS
__all__ = ["generateDS"]
